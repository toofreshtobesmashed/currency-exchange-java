package sk.murin.currency_exchange;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class Currency extends JFrame {
    private final Font f = new Font("Consolas", Font.BOLD, 23);
    private final String[] currencies = {"dolár $", "rubel ₱", "libra £", "czk Kč", "juan ¥", "Euro €"};
    private final Double[] rates = {1.198, 89.632, 0.875, 25.8, 7.75, 1.0};
    private final String[] symbols = {"$", "₱", "£", "Kč", "¥", "€"};
    private JLabel labelResult;
    private final Font fontLabels = new Font("Consolas", Font.BOLD, 17);
    private JTextField fieldSum;
    private JComboBox comboBoxCurrenciesFrom;
    private JComboBox comboBoxCurrenciesTo;

    public Currency() {
        setup();
    }

    private void setup() {

        setTitle("Exchange");
        setSize(400, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        setResizable(false);

        add(makePanelTop(), BorderLayout.NORTH);
        add(makePanelCenter(), BorderLayout.CENTER);
        add(makePanelBottom(), BorderLayout.SOUTH);

        setVisible(true);
    }


    private JPanel makePanelTop() {
        JPanel panel = new JPanel();
        JLabel labelMainMsg = new JLabel("Zmeň");
        labelMainMsg.setHorizontalAlignment(SwingConstants.CENTER);
        labelMainMsg.setFont(f);
        panel.add(labelMainMsg);
        return panel;
    }

    private JPanel makePanelCenter() {
        JPanel panel = new JPanel();
        panel.setLayout(null);

        JButton btnConvert = new JButton("Potvrď");
        btnConvert.setSize(150, 50);
        btnConvert.setLocation(120, 110);
        btnConvert.setFont(fontLabels);
        btnConvert.setFocusable(false);
        btnConvert.addActionListener(e -> convert());
        panel.add(btnConvert);


        JLabel labelSum = new JLabel("Aká suma");
        labelSum.setSize(75, 40);
        labelSum.setLocation(20, 14);
        labelSum.setFont(fontLabels);
        panel.add(labelSum);
        JLabel labelFrom = new JLabel("Z tejto meny");
        labelFrom.setSize(110, 40);
        labelFrom.setLocation(110, 14);
        labelFrom.setFont(fontLabels);
        panel.add(labelFrom);
        JLabel labelTo = new JLabel("Do tejto meny");
        labelTo.setSize(125, 40);
        labelTo.setLocation(235, 14);
        labelTo.setFont(fontLabels);
        panel.add(labelTo);

        comboBoxCurrenciesFrom = new JComboBox(currencies);
        comboBoxCurrenciesFrom.setSize(90, 30);
        comboBoxCurrenciesFrom.setLocation(120, 47);
        panel.add(comboBoxCurrenciesFrom);
        comboBoxCurrenciesTo = new JComboBox(currencies);
        comboBoxCurrenciesTo.setLocation(235, 14);
        comboBoxCurrenciesTo.setSize(90, 30);
        comboBoxCurrenciesTo.setLocation(250, 47);
        panel.add(comboBoxCurrenciesTo);

        fieldSum = new JTextField();
        fieldSum.setSize(70, 31);
        fieldSum.setLocation(20, 47);
        fieldSum.setFont(fontLabels);
        panel.add(fieldSum);
        return panel;
    }

    private JPanel makePanelBottom() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 3));
        labelResult = new JLabel("Je to: ");
        labelResult.setHorizontalAlignment(SwingConstants.CENTER);
        labelResult.setFont(fontLabels);
        labelResult.setBorder(new EmptyBorder(0, 5, 15, 5));
        panel.add(labelResult);

        return panel;
    }

    private void convert() {
        double value = 0;
        try {
            value = Double.parseDouble(fieldSum.getText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Nezadal si sumu","Zadaj sumu",JOptionPane.INFORMATION_MESSAGE);
        }

        int positionFrom = comboBoxCurrenciesFrom.getSelectedIndex();
        int positionTo = comboBoxCurrenciesTo.getSelectedIndex();
        int symbolPosition = comboBoxCurrenciesTo.getSelectedIndex();

        String symbol = symbols[symbolPosition];
        double rate = rates[positionTo] / rates[positionFrom];

        Double sum = rate * value;

        labelResult.setText(String.format("Je to %.2f%s podla kurzu %.2f", sum, symbol, rate));

    }
}
